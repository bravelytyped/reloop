const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const path = require('path');

module.exports = function(env, argv) {
  const plugins = [];

  env = env || {};

  if(env.opt) {
    plugins.push(new UglifyJsPlugin({
      sourceMap: true
    }));
  }

  return {
    entry: path.resolve(__dirname, 'src', 'reloop.js'),

    output: {
      path: path.resolve(__dirname, 'lib'),
      filename: env.opt ? 'reloop.min.js' : 'reloop.js',
      libraryTarget: 'umd',
      library: 'reloop'
    },

    module: {
      rules: [
        { test: /\.js$/, use: [ 'babel-loader' ] }
      ]
    },

    plugins: plugins,

    devServer: {
      contentBase: path.resolve(__dirname, 'example')
    }
  };
};
