const rafmock = require('./rafmock.js');
const reloop = require('../src/reloop.js');
const chai = require('chai');
const expect = chai.expect;

describe('reloop', () => {
  let raf = null;
  let loop = null;

  beforeEach(() => {
    raf = new rafmock();
    loop = new reloop(raf);
  });

  it('calls requestAnimationFrame when loop() is called', () => {
    loop.loop();
    expect(raf.pending_frames()).to.equal(1);
  });

  it('calls cancelAnimationFrame when stop() is called', () => {
    loop.loop();
    loop.stop();
    expect(raf.pending_frames()).to.equal(0);
  });

  it('calls requestAnimationFrame again after each frame', () => {
    loop.loop();
    raf.turn();
    expect(raf.pending_frames()).to.equal(1);
  });

  it('does not require any callbacks to be bound', () => {
    function testcase() {
      loop.loop();
      raf.turn();
      loop.stop();
    }

    expect(testcase).to.not.throw();
  });

  it('calls the load callback when started', () => {
    let flag = false;
    loop.load(() => { flag = true; });

    loop.loop();
    expect(flag).to.be.true;
  });

  it('calls the tick callback at a fixed rate', () => {
    let total_time = 0;
    loop.tick(dt => { total_time += dt; });
    raf.rate(40);
    loop.rate(50);

    loop.loop();
    raf.turn();
    raf.turn();
    expect(total_time).to.be.closeTo(2.0 / 50.0, 1e-4);
  });

  it('calls the draw callback exactly once per draw frame', () => {
    let draw_count = 0;
    loop.draw(lerp => { draw_count += 1; });

    loop.loop();
    raf.turn();
    expect(draw_count).to.equal(1);
  });

  it('calculates a lerp value based on the time remaining between two tick frames', () => {
    let save_lerp = 0;
    loop.draw(lerp => { save_lerp = lerp; });
    raf.rate(50);
    loop.rate(50);

    loop.loop();
    raf.turn();
    raf.turn();
    
    // 50ms -> 2 1/2 frames -> lerp should be 0.5
    expect(save_lerp).to.be.closeTo(0.5, 1e-4);
  });

  it('chains from loop()', () => {
    const loop_2 = loop.loop();
    expect(loop_2).to.equal(loop);
  });

  it('chains from stop()', () => {
    loop.loop();
    const loop_2 = loop.stop();
    expect(loop_2).to.equal(loop);
  });

  it('chains from load()', () => {
    const loop_2 = loop.load(null);
    expect(loop_2).to.equal(loop);
  });

  it('chains from tick()', () => {
    const loop_2 = loop.tick(null);
    expect(loop_2).to.equal(loop);
  });

  it('chains from draw()', () => {
    const loop_2 = loop.draw(null);
    expect(loop_2).to.equal(loop);
  });

  it('chains from rate()', () => {
    const loop_2 = loop.rate(30);
    expect(loop_2).to.equal(loop);
  });
});
