class rafmock {
  constructor() {
    this._frames = [];
    this._milli_time = 0;
    this._tick_rate = 200;
  }

  requestAnimationFrame(f) {
    this._frames.push(f);
  }

  cancelAnimationFrame(id) {
    this._frames.splice(id, 1);
  }

  rate(delta_millis) {
    this._tick_rate = delta_millis;
  }

  turn() {
    const frame = this._frames.shift();

    if(typeof frame === 'function') {
      frame(this._milli_time);
    }

    this._milli_time += this._tick_rate;
  }

  pending_frames() {
    return this._frames.length;
  }
}

module.exports = rafmock;
