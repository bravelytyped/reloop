class reloop {
  constructor(raf_impl) {
    this._raf_impl = raf_impl || window;

    this._load = () => null;
    this._tick = () => null;
    this._draw = () => null;

    this._rate = 60.0;
  }

  rate(n) {
    this._rate = n;
    return this;
  }

  load(f) {
    this._load = f;
    return this;
  }

  tick(f) {
    this._tick = f;
    return this;
  }

  draw(f) {
    this._draw = f;
    return this;
  }

  loop() {
    this._load();
    this._id = this._raf_impl.requestAnimationFrame(ts => this._first_frame(ts));
    return this;
  }

  _first_frame(ts) {
    this._ts = ts;
    this._dt = 0.0;
    this._frame(ts);
  }

  _frame(ts) {
    this._id = this._raf_impl.requestAnimationFrame(ts => this._frame(ts));

    this._dt += ts - this._ts;
    this._ts = ts;

    while(this._dt >= 1000.0 / this._rate) {
      this._dt -= 1000.0 / this._rate;
      this._tick(1.0 / this._rate);
    }

    const lerp = this._dt / 1000 * this._rate;
    this._draw(lerp);
  }

  stop() {
    this._raf_impl.cancelAnimationFrame(this._id);
    return this;
  }
}

module.exports = reloop;
