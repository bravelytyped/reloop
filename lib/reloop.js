(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["reloop"] = factory();
	else
		root["reloop"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var reloop = function () {
  function reloop(raf_impl) {
    _classCallCheck(this, reloop);

    this._raf_impl = raf_impl || window;

    this._load = function () {
      return null;
    };
    this._tick = function () {
      return null;
    };
    this._draw = function () {
      return null;
    };

    this._rate = 60.0;
  }

  _createClass(reloop, [{
    key: "rate",
    value: function rate(n) {
      this._rate = n;
      return this;
    }
  }, {
    key: "load",
    value: function load(f) {
      this._load = f;
      return this;
    }
  }, {
    key: "tick",
    value: function tick(f) {
      this._tick = f;
      return this;
    }
  }, {
    key: "draw",
    value: function draw(f) {
      this._draw = f;
      return this;
    }
  }, {
    key: "loop",
    value: function loop() {
      var _this = this;

      this._load();
      this._id = this._raf_impl.requestAnimationFrame(function (ts) {
        return _this._first_frame(ts);
      });
      return this;
    }
  }, {
    key: "_first_frame",
    value: function _first_frame(ts) {
      this._ts = ts;
      this._dt = 0.0;
      this._frame(ts);
    }
  }, {
    key: "_frame",
    value: function _frame(ts) {
      var _this2 = this;

      this._id = this._raf_impl.requestAnimationFrame(function (ts) {
        return _this2._frame(ts);
      });

      this._dt += ts - this._ts;
      this._ts = ts;

      while (this._dt >= 1000.0 / this._rate) {
        this._dt -= 1000.0 / this._rate;
        this._tick(1.0 / this._rate);
      }

      var lerp = this._dt / 1000 * this._rate;
      this._draw(lerp);
    }
  }, {
    key: "stop",
    value: function stop() {
      this._raf_impl.cancelAnimationFrame(this._id);
      return this;
    }
  }]);

  return reloop;
}();

module.exports = reloop;

/***/ })
/******/ ]);
});