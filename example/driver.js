(function(document, reloop) {

  // Canvas setup.
  var canvas = document.createElement('canvas');
  document.body.appendChild(canvas);

  canvas.width = canvas.scrollWidth;
  canvas.height = canvas.scrollHeight;

  var ctx = canvas.getContext('2d');

  // Declare state variables.
  var t0;
  var t1;

  // Create the loop instance.
  var loop = new reloop();

  // Configure the loop.
  loop
    .rate(30)
    .load(function() {
      // Initialize previous and current state.
      t0 = 0;
      t1 = 0;
    })
    .tick(function(dt) {
      // Update state using fixed dt value (1.0 / rate).
      t0 = t1;
      t1 += dt;
    })
    .draw(function(lerp) {
      // Clear screen.
      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, canvas.width, canvas.height);

      // Translate to center of screen.
      ctx.save();
      ctx.translate(canvas.width / 2, canvas.height / 2);

      // Interpolate between current state and previous state.
      var lerp_t = lerp * t1 + (1.0 - lerp) * t0;

      // Calculate rendering parameters based on interpolated state.
      var x = Math.cos(lerp_t) * 100 - 25;
      var y = -Math.sin(lerp_t) * 100 - 25;

      // Draw rectangle.
      ctx.fillStyle = 'blue';
      ctx.fillRect(x, y, 50, 50);

      // Revert graphics transform.
      ctx.restore();
    });

  // Start the loop.
  loop.loop();

})(document, reloop);
